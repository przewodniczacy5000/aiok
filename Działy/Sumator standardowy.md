# Sumator jednobitowy robiony naiwnie
![std](./static/sumator%20std.png)

Gwoli ścisłości - $f_1$ to przeniesienie, $f_0$ wynik sumy.

![std1](./static/sumator%20std1.png)

[[Optymalizacja]]# się tutaj kłania.

![std2](./static/sumator%20std2.png)

![std3](./static/sumator%20std3.png)

