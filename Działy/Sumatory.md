---
tags:
    - sumator
    - technikacyfrowa
---
# Sum Ator(y) jest krulem dżungli

**Wejściówka:** [[WejSumatory]]#

## wersja wykładowa

### Sumator w sosie klasycznym
[[Sumator standardowy]]#
### Sumator jednobitowy z półsumatorów
#### Półsumator
![ps1](./static/polsumator.png)

$g_1$ to przeniesienie, $g_0$ jest sumą.
#### Sumator
![ps2](./static/sumator%20z%20ps.png)

przez c+ rozumiem przeniesienie wynikowe, bn - wynik działania funkcji
### Sumator trójbitowy
![trzybit](./static/trzybit.png)

Ja będąc otumaniony jakością zajęć zbaraniałem, więc dopiszę sobie opis:
W tym sumatorze mamy dwie trzybitowe _liczby_ $a_2a_1a_0$ i $b_2b_1b_0$. w wyniku dodania mamy $p_2w_2w_1w_0$.
## Techniki wykorzystywane w tym dziale:
- [[Optymalizacja]]#



