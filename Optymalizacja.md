# Optymalizacja a'la UWM

![Tabelka](./static/tabelka.png)

## podejście pesymistyczne(przez zera)

1. bądź pętelką przechodzącą przez górę po lewej
2. daj się spisać potęznemu studentowi:

![Tabela2](./static/libre1.png)

3. dajesz sb skreślić zmienne(w naszym przypadu x)
4. spisujemy pozostałe zmienne: jeżeli na krawędziach mapy są zera - wtedy **nie ma** negacji, a gdy po lewej lub na górze jedynka będzie - *wtedy negacja się należy*. por 2 wyraz i  drugie oczko z $f_0$ w powyższej tabeli

![std2](./static/sumator%20std2.png)

**Intuicja.** f1 ma nam wygenerować same zera - jeżeli to się nie uda, to przez właściwości funkcji bulowskiej jest gituwa i tak



#sumatory #technikacyfrowa